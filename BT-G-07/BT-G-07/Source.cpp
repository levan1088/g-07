﻿#include "DaThuc.h"

void main()
{
	DaThuc f = ReadFile("F1.txt");           // Đoc file và lấy dữ liệu đa thuc F1 gán vào DaThuc f
	DaThuc p = ReadFile("F2.txt");            // Đoc file và lấy dữ liệu đa thuc F2 gán vào DaThuc p
	fstream h;
	char *out = "F-result.txt";
	h.open(out, ios_base::out);
	
	ShortenPolynomial(f);                     // Rút gọn DaThuc f
	ShortenPolynomial(p);                     // Rút gọn DaThuc p

	StandardizePolynomial(f);                 // Chuẩn hóa DaThuc f
	StandardizePolynomial(p);                 // Chuẩn hóa DaThuc p

	h << "F1" << endl;
	WritePolynomialToFile(h, f);            // In DaThuc f (F1) vào file
	h << endl << endl << "F2" << endl;
	WritePolynomialToFile(h, p);            // In DaThuc p (F2) vào file

	DaThuc cong = f + p;
	DaThuc tru = f - p;
	DaThuc nhan = f * p;

	h << endl << endl << "F3" << endl;
	WritePolynomialToFile(h, cong);        // In kết quả phép cộng 2 DaThuc vào file
	h << endl << endl << "F4" << endl;
	WritePolynomialToFile(h, tru);         // In kết quả phép trừ 2 DaThuc vào file
	h << endl << endl << "F5" << endl;
	WritePolynomialToFile(h, nhan);        // In kết quả phép nhân 2 DaThuc vào file

	h.close();
}