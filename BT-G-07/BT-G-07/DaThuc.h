﻿#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
using namespace std;

/*
	Các hàm và phương thức ở bài G-2 ghi bằng tiếng Việt được chuyển thành tiếng Anh ở bài G-3
*/

// DaThuc được hình thành từ các DonThuc cộng lại
struct DonThuc
{
	float a;
	unsigned int x1;
	unsigned int mu1;
	unsigned int x2 = 0;	// Giá trị mặc định của x2 và mu2, khi nhân 2 DaThuc sẽ được cập nhật lại nếu x khác nhau
	unsigned int mu2 = 0;
};

struct NODE
{
	DonThuc data;
	NODE* pNext;
};

struct DaThuc		// Danh sách các DonThuc trong 1 DaThuc gồm n phần tử
{
	NODE *pHead;
	NODE *pTail;
};

DonThuc operator*(DonThuc x, DonThuc y);		          // Nhân 2 DonThuc và trả về kết quả 1 DonThuc
bool operator==(DonThuc a, DonThuc b);			          // So sánh 2 DonThuc (so sánh số mũ, x, hệ số)
DaThuc& operator+(DaThuc x, DaThuc y);			          // Cộng 2 DaThuc và trả về kết quả 1 DaThuc
DaThuc& operator-(DaThuc x, DaThuc y);			          // Trừ 2 DaThuc và trả về kết quả 1 DaThuc
DaThuc& operator*(DaThuc x, DaThuc y);			          // Nhân 2 DaThuc và trả về kết quả 1 DaThuc

bool Match(DonThuc a, DonThuc b);				          // So sánh 2 DonThuc (so sánh số mũ, x)
void Swap(DonThuc &a, DonThuc &b);				          // Hoán đổi 2 DonThuc

DaThuc& ReadFile(char *FileName);		                  // Hàm đọc vào 1 DaThuc từ file và trả về kết quả 1 DaThuc
DaThuc& Duplicate(const DaThuc &a);				          // Copy 1 DaThuc
void PolynomialPrinting(DaThuc dt);						  // Hàm xuất 1 DaThuc ra màn hình
void StandardizePolynomial(DaThuc &dt);				      // Hàm chuẩn hóa 1 DaThuc (sắp xếp các DonThuc theo số mũ từ thấp đến cao)
void ShortenPolynomial(DaThuc &dt);					      // Hàm rút gọn 1 DaThuc (cộng các DonThuc có cùng số mũ và cùng x)
void WritePolynomialToFile(fstream& f, DaThuc dt);        // Hàm in DaThuc ra file

void Init(DaThuc &dt);							          // Khởi tạo 1 DaThuc rỗng
void AddLast(DaThuc &dt, NODE *p);				          // Thêm 1 DonThuc vào cuối list (DaThuc)
void DeleteFirst(DaThuc &dt);					          // Xóa 1 DonThuc ở đầu 1 list (DaThuc)
void DeleteLast(DaThuc &dt);					          // Xóa 1 DonThuc ở cuối list (DaThuc)
void DeleteNode(DaThuc &dt, DonThuc z);			          // Xóa 1 DonThuc == với 1 DonThuc z
NODE* CreateNode(DonThuc z);					          // Tạo 1 NODE có dữ liệu là 1 DonThuc