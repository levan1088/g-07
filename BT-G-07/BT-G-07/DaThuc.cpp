﻿#include "DaThuc.h"



/*
	So sánh 2 DonThuc (so sánh số mũ, x, hệ số)
	Input: DonThuc a, DonThuc b
	Return: true - bằng nhau
			false - không bằng nhau
*/
bool operator==(DonThuc a, DonThuc b)
{
	if ((a.a == b.a && a.x1 == b.x1 && a.mu1 == b.mu1 && a.x2 == b.x2 && a.mu2 == b.mu2) || (a.a == b.a && a.x1 == b.x2 && a.mu1 == b.mu2 && a.x2 == b.x1 && a.mu2 == b.mu1))
		return true;
	return false;
}

/*
	Hàm so sánh 2 DonThuc (so sánh số mũ, x)
	Input: DonThuc a, DonThuc b
	Return: true - bằng nhau
			false - không bằng nhau
*/
bool Match(DonThuc a, DonThuc b)
{
	if ((a.x1 == b.x1 && a.mu1 == b.mu1 && a.x2 == b.x2 && a.mu2 == b.mu2) || (a.x1 == b.x2 && a.mu1 == b.mu2 && a.x2 == b.x1 && a.mu2 == b.mu1))
		return true;
	return false;
}

/*
	Nhân 2 DonThuc và trả về kết quả 1 DonThuc
	Input: DonThuc x, DonThuc y
	Return: DonThuc
*/
DonThuc operator*(DonThuc x, DonThuc y)	// Nhân 2 DonThuc, nếu cùng x thì nhân 2 hệ số và cộng số mũ lại; nếu khác x thì nhân 2 hệ số, gán x và số mũ vào x1, mu1, x2, mu2
{
	DonThuc res;
	if (x.x1 == y.x1)
	{
		res.a = x.a * y.a;
		res.x1 = x.x1;
		res.mu1 = x.mu1 + y.mu1;
	}
	else
	{
		res.a = x.a * y.a;
		res.x1 = x.x1;
		res.x2 = y.x1;
		res.mu1 = x.mu1;
		res.mu2 = y.mu1;
	}
	return res;
}

/*
	 Hàm khởi tạo 1 DaThuc rỗng
	 Input: DaThuc dt
	 Result: DaThuc dt được khởi tạo
*/
void Init(DaThuc &dt)
{
	dt.pHead = dt.pTail = NULL;
}

/*
	Hàm tạo 1 NODE có dữ liệu là 1 DonThuc
	Input: DonThuc z
	Return: NODE
*/
NODE* CreateNode(DonThuc z)		
{
	NODE *p = new NODE;
	p->data.a = z.a;
	p->data.mu1 = z.mu1;
	p->data.mu2 = z.mu2;
	p->data.x1 = z.x1;
	p->data.x2 = z.x2;
	p->pNext = NULL;
	return p;
}

/*
	Hàm thêm 1 DonThuc vào cuối list (DaThuc)
	Input: DaThuc dt, NODE *p
	Result: DaThuc dt có thêm 1 NODE
*/
void AddLast(DaThuc &dt, NODE *p)
{
	if (dt.pHead == NULL)
		dt.pHead = dt.pTail = p;
	else
	{
		dt.pTail->pNext = p;
		dt.pTail = p;
	}
}

/*
	Hàm xóa 1 DonThuc ở đầu 1 list (DaThuc)
	Input: DaThuc dt
	Result: DaThuc dt sau khi xóa
*/
void DeleteFirst(DaThuc &dt)	
{
	if (dt.pHead == NULL)
		return;
	else
		if (dt.pHead == dt.pTail)
			dt.pHead = dt.pTail = NULL;
		else
		{
			NODE *p = dt.pHead;
			dt.pHead = dt.pHead->pNext;
			p->pNext = NULL;
			delete p;
			p = NULL;
		}
}

/*
	Hàm xóa 1 DonThuc ở cuối list (DaThuc)
	Input: DaThuc dt
	Result: DaThuc dt sau khi xóa 
*/
void DeleteLast(DaThuc &dt)
{
	if (dt.pHead == NULL)
		return;
	else
		if (dt.pHead == dt.pTail)
			dt.pHead = dt.pTail = NULL;
		else
		{
			NODE *p = dt.pHead;
			while (p->pNext != dt.pTail)
				p = p->pNext;

			NODE *t = dt.pTail;
			p->pNext = NULL;
			dt.pTail = p;
			delete t;
			t = NULL;
		}
}

/*
	Hàm xóa 1 DonThuc == với 1 DonThuc z
	Input: DaThuc dt, DonThuc z
	Result: DaThuc dt sau khi xóa DonThuc z
*/
void DeleteNode(DaThuc &dt, DonThuc z)
{
	if (dt.pHead == NULL)
		return;
	else
		if (dt.pHead->data == z)
			DeleteFirst(dt);
		else
			if (dt.pTail->data == z)
				DeleteLast(dt);
			else
			{
				for(NODE *p = dt.pHead; p != NULL; p = p->pNext)
					if (p->pNext->data == z)
					{
						NODE *q = p;
						NODE *temp = q->pNext;
						q->pNext = temp->pNext;
						temp->pNext = NULL;
						delete temp;
						temp = NULL;
						break;
					}
			}
}

/*
Hàm copy 1 DaThuc
Input: DaThuc a
Return: DaThuc
*/
DaThuc& Duplicate(const DaThuc &a)
{
	DaThuc res;
	Init(res);

	for (NODE *p = a.pHead; p != NULL; p = p->pNext)
		AddLast(res, CreateNode(p->data));

	return res;
}

//----------------------------------------------------------CÂU i------------------------------------------------------------

/*
	Hàm đọc vào 1 DaThuc từ file và trả về kết quả 1 DaThuc
	Input: FileName
	Return: DaThuc
*/
DaThuc& ReadFile(char *FileName)
{
	DaThuc res;
	Init(res);

	unsigned int x;
	unsigned int mu;
	float a;
	bool sign = false;

	fstream f;
	f.open(FileName, ios_base::in);
	string s, temp = "";
	getline(f, s);
	s += '+';

	int i = 0;
	int j = i;
	if (s[i] == '-')	// Kiểm tra hệ số đầu tiên có phải là số âm không?
	{
		temp += s[i];
		i++;
	}

	while (i < s.length())
	{	
		if (s[i] == '*')	// Khi gặp dấu * thì chuyển đổi chuỗi temp đọc được trước đó thành hệ số a
		{
			a = stof(temp);
			temp = "";
		}
		else
			if (s[i] == '^')	// Khi gặp dấu ^ thì chuyển đổi chuỗi temp đọc được thành x
			{
				x = stoul(temp);
				temp = "";
				sign = true;	// Cho biết DonThuc có đầy đủ thành phần, khác trường hợp số mũ = 1
			}
			else
				if (s[i] == '-' || s[i] == '+')	// Khi gặp dấu + hoặc - thì chuyển đổi chuỗi temp thành mu nếu mu != 1, không thì chuyển temp thành x và mu = 1
				{
					if (sign == false)
					{
						x = stoul(temp);
						temp = "";
						temp += s[i];
						mu = 1;	
					}
					else
					{
						mu = stoul(temp);
						temp = "";
						temp += s[i];
					}

					DonThuc p;
					p.a = a;
					p.mu1 = mu;
					p.x1 = x;
					AddLast(res, CreateNode(p));
					sign = false;
				}
				else
					temp += s[i];
		i++;
	}


	f.close();
	return res;
}

/*
	Hàm xuất 1 DaThuc ra màn hình
	Input: DaThuc dt
	Result: In DaThuc dt ra màn hình
*/
void PolynomialPrinting(DaThuc dt)
{	
	if (dt.pHead->data.a < 0)
		cout << '-';
	for (NODE *p = dt.pHead; p != NULL; p = p->pNext)
	{	
		if (p != dt.pTail)
		{
			cout << fabs(p->data.a) << "*" << p->data.x1;
			if (p->data.mu1 != 1)
				cout << "^" << p->data.mu1;
			if (p->data.x2 != 0 && p->data.mu2 != 0)
			{
				if (p->data.mu2 != 1)
					cout << "*" << p->data.x2 << "^" << p->data.mu2;
				else
					cout << "*" << p->data.x2;
			}
			if (p->pNext->data.a < 0)
				cout << " - ";
			else
				cout << " + ";
		}
		else
		{
			cout << fabs(p->data.a) << "*" << p->data.x1;
			if (p->data.mu1 != 1)
				cout << "^" << p->data.mu1;
			if (p->data.x2 != 0 && p->data.mu2 != 0)
			{
				if (p->data.mu2 != 1)
					cout << "*" << p->data.x2 << "^" << p->data.mu2;
				else
					cout << "*" << p->data.x2;
			}
		}
	}
}

//----------------------------------------------------------CÂU ii------------------------------------------------------------

/*
	Hàm rút gọn 1 DaThuc (cộng các DonThuc có cùng số mũ và cùng x)
	Input: DaThuc dt
	Result: DaThuc dt được rút gọn
*/
void ShortenPolynomial(DaThuc &dt)
{
	for (NODE *p = dt.pHead; p != NULL;)	// Kiểm tra trong DaThuc có DonThuc nào có hệ số a = 0 thì xóa
	{
		if (p->data.a == 0)
		{
			DonThuc x = p->data;
			p = p->pNext;
			DeleteNode(dt, x);
		}
		else
			p = p->pNext;
	}

	for (NODE *p = dt.pHead; p != NULL; p = p->pNext)	// Kiểm tra trong DaThuc, nếu có DonThuc trùng x với mu thì rút gọn lại thành 1 DonThuc bằng cách cộng hệ số lại
	{
		for (NODE *q = p->pNext; q != NULL;)
		{
			if (Match(p->data, q->data))
			{
				DonThuc x = q->data;
				p->data.a += q->data.a;
				q = q->pNext;
				DeleteNode(dt, x);
			}
			else
				q = q->pNext;
		}
	}
}

//----------------------------------------------------------CÂU iii------------------------------------------------------------

/*
	 Hàm hoán đổi 2 DonThuc
	 Input: DonThuc a, DonThuc b
	 Result: DonThuc a và DonThuc b hoán đổi giá trị cho nhau
*/
void Swap(DonThuc &a, DonThuc &b)
{
	DonThuc temp = a;
	a = b;
	b = temp;
}

/*
	Hàm chuẩn hóa 1 DaThuc (sắp xếp các DonThuc theo số mũ từ thấp đến cao, nếu cùng số mũ thì sắp xếp theo giá trị x)
	Intput: DaThuc dt
	Result: DaThuc dt đã được chuẩn hóa
*/
void StandardizePolynomial(DaThuc &dt)
{
	for (NODE *p = dt.pHead; p != NULL; p = p->pNext)	// Sắp xếp các DonThuc theo thứ tự tăng dần của số mũ
		for (NODE *q = p->pNext; q != NULL; q = q->pNext)
			if (p->data.mu1 > q->data.mu1)
				Swap(p->data, q->data);

	for (NODE *p = dt.pHead; p != dt.pTail;)		// Nếu số mũ bằng nhau thì sắp xếp các DonThuc có thứ tự tăng dần theo x
		if (p->data.mu1 == p->pNext->data.mu1 && p->data.x1 > p->pNext->data.x1)
		{
			Swap(p->data, p->pNext->data);
			p = dt.pHead;
		}
		else
			p = p->pNext;
}

//----------------------------------------------------------CÂU iv------------------------------------------------------------

/*
	Hàm in DaThuc ra file
	Input: fstream f, DaThuc dt
	Result: Xuất DaThuc dt ra file
*/
void WritePolynomialToFile(fstream& f, DaThuc dt)
{
	if (dt.pHead->data.a < 0)
		f << '-';
	f << fixed;
	for (NODE *p = dt.pHead; p != NULL; p = p->pNext)
	{
		if (p != dt.pTail)
		{
			f << setprecision(2) << fabs(p->data.a) << "*" << p->data.x1;
			if (p->data.mu1 != 1)
				f << "^" << p->data.mu1;
			if (p->data.x2 != 0 && p->data.mu2 != 0)
			{
				if (p->data.mu2 != 1)
					f << "*" << p->data.x2 << "^" << p->data.mu2;
				else
					f << "*" << p->data.x2;
			}
			if (p->pNext->data.a < 0)
				f << "-";
			else
				f << "+";
		}
		else
		{
			f << setprecision(2) << fabs(p->data.a) << "*" << p->data.x1;
			if (p->data.mu1 != 1)
				f << "^" << p->data.mu1;
			if (p->data.x2 != 0 && p->data.mu2 != 0)
			{
				if (p->data.mu2 != 1)
					f << "*" << p->data.x2 << "^" << p->data.mu2;
				else
					f << "*" << p->data.x2;
			}
		}
	}

}

//----------------------------------------------------------CÂU v------------------------------------------------------------

/*
	Cộng 2 DaThuc và trả về kết quả 1 DaThuc
	Input: DaThuc x, DaThuc y
	Return: DaThuc - Kết quả
*/
DaThuc& operator+(DaThuc x, DaThuc y)
{
	DaThuc res;
	Init(res);

	DaThuc a = Duplicate(x);	// Copy 2 DaThuc sang 2 DaThuc khác để không ảnh hưởng đến DaThuc đầu vào
	DaThuc b = Duplicate(y);
	for (NODE *p = a.pHead; p != NULL; p = p->pNext)	// Duyệt DaThuc a và DaThuc b, nếu có DonThuc nào trong DaThuc b trùng x và mu với DonThuc trong DaThuc a thì cộng lại và add vào DaThuc res
	{													
		for (NODE *q = b.pHead; q != NULL;)
		if (Match(p->data, q->data))
		{
			p->data.a += q->data.a;
			DonThuc temp = q->data;
			q = q->pNext;
			DeleteNode(b, temp);
		}
		else
			q = q->pNext;
		AddLast(res, CreateNode(p->data));
	}

	for (NODE *p = b.pHead; p != NULL; p = p->pNext)	// Add những DonThuc còn lại trong DaThuc b vào DaThuc res
		AddLast(res, CreateNode(p->data));

	ShortenPolynomial(res);     // Thực hiện rút gọn DaThuc res
	StandardizePolynomial(res); // Thực hiện chuẩn hóa DaThuc res
	return res;
}

//----------------------------------------------------------CÂU vi------------------------------------------------------------//

/*
	Trừ 2 DaThuc và trả về kết quả 1 DaThuc
	Input: DaThuc x, DaThuc y
	Return: DaThuc - Kết quả
*/
DaThuc& operator-(DaThuc x, DaThuc y)
{
	DaThuc res;
	Init(res);

	DaThuc a = Duplicate(x);	// Copy 2 DaThuc sang 2 DaThuc khác để không ảnh hưởng đến DaThuc đầu vào	
	DaThuc b = Duplicate(y);

	for (NODE *p = b.pHead; p != NULL; p = p->pNext)	// Nhân -1 cho hệ số các DonThuc trong DaThuc b, sau đó thực hiện phép cộng 2 DaThuc a và DaThuc b
		p->data.a *= -1;

	for (NODE *p = a.pHead; p != NULL; p = p->pNext)
	{
		for (NODE *q = b.pHead; q != NULL;)
			if (Match(p->data, q->data))
			{
				p->data.a += q->data.a;
				DonThuc temp = q->data;
				q = q->pNext;
				DeleteNode(b, temp);
			}
			else
				q = q->pNext;
		AddLast(res, CreateNode(p->data));
	}

	for (NODE *p = b.pHead; p != NULL; p = p->pNext)	// Add những DonThuc còn lại trong DaThuc b vào DaThuc res
		AddLast(res, CreateNode(p->data));

	ShortenPolynomial(res);       // Thực hiện rút gọn DaThuc res
	StandardizePolynomial(res);   // Thực hiện chuẩn hóa DaThuc res
	return res;
}

//----------------------------------------------------------CÂU vii------------------------------------------------------------

/*
	Nhân 2 DaThuc và trả về kết quả 1 DaThuc
	Input: DaThuc x, DaThuc y
	Return: DaThuc - Kết quả
*/
DaThuc& operator*(DaThuc x, DaThuc y)
{
	DaThuc res;
	Init(res);

	for (NODE *p = x.pHead; p != NULL; p = p->pNext)	// Nhân lần lượt các DonThuc trong DaThuc x cho các DonThuc trong DaThuc y và add vào res (DaThuc chứa kết quả).
		for (NODE *q = y.pHead; q != NULL; q = q->pNext)
			AddLast(res, CreateNode(p->data * q->data));

	ShortenPolynomial(res);	    // Thực hiện rút gọn DaThuc res
	StandardizePolynomial(res); // Thực hiện chuẩn hóa DaThuc res
	return res;
}
