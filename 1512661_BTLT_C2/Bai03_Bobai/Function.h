#include <stdio.h>
#include <conio.h>
#include <queue>
using namespace std;

//Struct NODE
struct NODE
{
	int n;
	NODE* pNext;
};

//Struct LIST
struct LIST
{
	NODE *pHead;
	NODE *pTail;
};
NODE *GetNode(int x);
void Init(LIST &l);
bool isEmpty(LIST l);
void Push(LIST &l,int n);
int DelFirst(LIST &l);
void LastCard(LIST &l);